# git go binary

## General information

* **Author**: ldawert
* **Description**: This is a short binary that stages, commits and pushes all changes to current branch. It interactively queries a commit message.

## Usage

* Clone this repository
* Move, link or copy binary `git-go` to a location from your `$PATH` variable (make sure it's executable)
* Enjoy speed-up in git workflow

```shell
$ cd /git/project  # change to git dir
$ vi testfile      # do changes
$ git go           # use git-go binary
This will add, commit and push all changes in current directory /git/project to branch main
Please enter your commit message: awesome commit message

[main abcdefgh] awesome commit message
 1 file changed, 1 insertion(+), 1 deletion(-)
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 337 bytes | 337.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
To gitlab.example.com:user/project.git
   qrstuvwxyz..abcdefgh  main -> main
```
